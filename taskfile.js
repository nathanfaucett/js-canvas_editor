var task = require("@nathanfaucett/task"),
    jshint = require("gulp-jshint"),
    jsbeautifier = require("gulp-jsbeautifier"),
    del = require("del"),
    connect = require("gulp-connect"),
    webpack = require("webpack"),
    webpackConfig = require("./webpack.config.js");


var port = 8080,
    reloadPort = 35729;


task("jsbeautifier", "beautifier js files", task.parallel(
    function taskfile() {
        return task.src("./taskfile.js").pipe(jsbeautifier()).pipe(task.dest("."));
    },
    function src() {
        return task.src("./src/**/*.js").pipe(jsbeautifier()).pipe(task.dest("./src"));
    },
    function test() {
        return task.src("./test/**/*.js").pipe(jsbeautifier()).pipe(task.dest("./test"));
    }
));

task("jshint", "run jshint", function jsh() {
    return (
        task.src([
            "./taskfile.js",
            "./src/**/*.js",
            "./test/**/*.js"
        ])
        .pipe(jshint({
            es3: true,
            unused: true,
            curly: true,
            eqeqeq: true,
            expr: true,
            eqnull: true,
            proto: true
        }))
        .pipe(jshint.reporter("default"))
    );
});

task("default", task.series(
    task("jsbeautifier"),
    task("jshint")
));

task("clean", function() {
    return del([
        "./examples/**/*.js",
        "!./examples/**/src/*.js"
    ]);
});

task("build", function(callback) {
    webpack(
        webpackConfig,
        function onCompile(error, stats) {
            console.log(stats.toString({
                chunks: true,
                colors: true
            }));
        }
    );
    callback();
});

task("serve", function() {
    connect.server({
        port: port,
        livereload: {
            port: reloadPort
        }
    });
});

task("reload", function() {
    return task.src([
        "./examples/**/*.js",
        "!./examples/**/src/*.js"
    ]).pipe(connect.reload());
});

task("watch", function(done) {
    task.watch([
            "./examples/**/*.js",
            "!./examples/**/src/*.js"
        ],
        task("reload")
    );
    done();
});

task("run", task.parallel(task("clean"), task("build"), task("serve"), task("watch")));