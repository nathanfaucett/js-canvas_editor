var vec2 = require("@nathanfaucett/vec2"),
    aabb2 = require("@nathanfaucett/aabb2"),
    EventEmitter = require("@nathanfaucett/event_emitter"),
    tools = require("./enums/tools"),
    states = require("./enums/states"),
    defaults = require("./enums/defaults");


var PointersPrototype;


module.exports = Pointers;


function Pointers() {

    EventEmitter.call(this, -1);

    this._toolStates = {};
    this._pointers = {};
}
EventEmitter.extend(Pointers);
PointersPrototype = Pointers.prototype;

Pointers.create = function() {
    return (new Pointers()).construct();
};

PointersPrototype.construct = function() {
    return this;
};

PointersPrototype.destructor = function() {

    this.removeAllListeners();

    return this;
};

PointersPrototype.setPointerPosition = function(id, x, y) {
    var pointer = getPointer(this, id),
        toolState = getToolState(this, id),
        position = pointer.position,
        lastX = position[0],
        lastY = position[1];

    vec2.copy(toolState.last, pointer.position);

    position[0] = x;
    position[1] = y;

    aabb2.expandPoint(toolState.aabb, toolState.aabb, position);

    if (lastX !== x || lastY !== y) {
        this.emit("pointer-move", pointer, toolState);

        if (toolState.state === states.START) {
            toolState.state = states.RUN;
            this.emit("pointer-state", pointer, toolState);
        } else if (toolState.state === states.RUN) {
            this.emit("pointer-state", pointer, toolState);
        }
    }

    return this;
};

PointersPrototype.setPointerForce = function(id, force) {
    var pointer = getPointer(this, id),
        toolState = getToolState(this, id),
        isDown = pointer.force === 0 && force > 0,
        isUp = pointer.force > 0 && force === 0;

    if (isDown) {
        this.emit("pointer-down", pointer, toolState);
    } else if (isUp) {
        this.emit("pointer-up", pointer, toolState);
    }

    switch (toolState.tool) {

        case tools.BRUSH:
            if (toolState.state === states.NONE && isDown) {
                pointerStart(this, pointer, toolState);
            } else if (pointer.state !== states.NONE && isUp) {
                pointerEnd(this, pointer, toolState);
            }
            break;

        case tools.LINE:
            if (isDown) {
                if (toolState.state === states.NONE) {
                    pointerStart(this, pointer, toolState);
                } else if (pointer.state !== states.NONE) {
                    pointerEnd(this, pointer, toolState);
                }
            }
            break;
    }

    pointer.force = force;

    return this;
};

PointersPrototype.setPointerCancel = function(id) {
    var pointer = getPointer(this, id),
        toolState = getToolState(this, id);

    pointer.force = 0;
    pointerEnd(this, pointer, toolState);

    return this;
};

PointersPrototype.setPointerTool = function(id, tool) {
    var pointer = getPointer(this, id),
        toolState = getToolState(this, id);

    if (toolState.state === states.NONE && toolState.tool !== tool) {
        this.emit("pointer-tool", pointer, toolState);
    }

    toolState.tool = tool;

    return this;
};


function pointerStart(_this, pointer, toolState) {
    if (toolState.state === states.NONE) {

        toolState.state = states.START;
        vec2.copy(toolState.start, pointer.position);
        aabb2.clear(toolState.aabb);

        _this.emit("pointer-state", pointer, toolState);
    }
}

function pointerEnd(_this, pointer, toolState) {
    if (toolState.state !== states.NONE) {

        toolState.state = states.END;
        vec2.copy(toolState.end, pointer.position);

        _this.emit("pointer-state", pointer, toolState);

        process.nextTick(function onNextTick() {
            toolState.state = states.NONE;
            _this.emit("pointer-state", pointer, toolState);
        });
    }
}


function ToolState(id) {
    this.id = id;
    this.state = states.NONE;

    this.tool = defaults.tool;
    this.width = defaults.width;
    this.color = defaults.color;

    this.start = vec2.create(0, 0);
    this.end = vec2.create(0, 0);
    this.last = vec2.create(0, 0);

    this.aabb = aabb2.create();
}

function getToolState(_this, id) {
    return _this._toolStates[id] || (_this._toolStates[id] = new ToolState(id));
}

function Pointer(id) {
    this.id = id;
    this.force = 0;
    this.position = vec2.create(0, 0);
}

function getPointer(_this, id) {
    return _this._pointers[id] || (_this._pointers[id] = new Pointer(id));
}