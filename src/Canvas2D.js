var Canvas = require("./Canvas");


var CanvasPrototype = Canvas.prototype,
    Canvas2DPrototype;


module.exports = Canvas2D;


function Canvas2D() {

    Canvas.call(this);

    this.ctx = null;
}
Canvas.extend(Canvas2D, "canvas_editor.Canvas2D");
Canvas2DPrototype = Canvas2D.prototype;

Canvas2DPrototype.construct = function(document, w, h) {

    CanvasPrototype.construct.call(this, document, w, h);

    this.ctx = this.element.getContext("2d");

    return this;
};

Canvas2DPrototype.destructor = function() {

    CanvasPrototype.destructor.call(this, document, w, h);

    this.ctx = null;

    return this;
};

Canvas2DPrototype.drawImage = function(img, x, y, w, h) {
    this.ctx.drawImage(img, x, y, w, h);
    return this;
};

Canvas2DPrototype.putImageData = function(data, x, y) {
    this.ctx.putImageData(data, x, y);
    return this;
};

Canvas2DPrototype.toDataURL = function() {
    return this.element.toDataURL();
};

Canvas2DPrototype.getImageData = function(x, y, w, h) {
    return this.ctx.getImageData(x, y, w, h);
};

Canvas2DPrototype.drawLine = function(startX, startY, endX, endY, width, color) {
    var ctx = this.ctx;

    ctx.beginPath();
    ctx.lineCap = "round";
    ctx.lineWidth = width;
    ctx.strokeStyle = color;
    ctx.moveTo(startX, startY);
    ctx.lineTo(endX, endY);
    ctx.stroke();

    return this;
};

Canvas2DPrototype.clear = function() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    return this;
};