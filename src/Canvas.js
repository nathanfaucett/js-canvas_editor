var Class = require("@nathanfaucett/class");


var ClassPrototype = Class.prototype,
    CanvasPrototype;


module.exports = Canvas;


function Canvas() {

    Class.call(this);

    this.element = null;
    this.width = 0;
    this.height = 0;
}
Class.extend(Canvas, "canvas_editor.Canvas");
CanvasPrototype = Canvas.prototype;

CanvasPrototype.construct = function(document, w, h) {
    var canvas = document.createElement("canvas"),
        canvasStyle = canvas.style;

    canvasStyle.position = "absolute";
    canvasStyle.top = "0px";
    canvasStyle.left = "0px";

    ClassPrototype.construct.call(this);

    this.element = canvas;
    this.resize(w, h);

    return this;
};

CanvasPrototype.destructor = function() {

    ClassPrototype.destructor.call(this);

    this.element = null;
    this.width = 0;
    this.height = 0;

    return this;
};

CanvasPrototype.resize = function(w, h) {
    var canvas = this.element,
        canvasStyle = canvas.style;

    this.width = w;
    this.height = h;

    canvasStyle.width = this.width + "px";
    canvasStyle.height = this.height + "px";
    canvas.width = this.width;
    canvas.height = this.height;

    return this;
};