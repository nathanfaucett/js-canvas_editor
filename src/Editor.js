var isNode = require("@nathanfaucett/is_node"),
    debounce = require("@nathanfaucett/debounce"),
    vec4 = require("@nathanfaucett/vec4"),
    Class = require("@nathanfaucett/class"),
    sceneGraph = require("@nathanfaucett/scene_graph"),
    sceneRenderer = require("@nathanfaucett/scene_renderer"),
    cameraComponent = require("@nathanfaucett/camera_component"),
    transformComponents = require("@nathanfaucett/transform_components"),
    spriteComponent = require("@nathanfaucett/sprite_component"),
    Shader = require("@nathanfaucett/shader"),
    Material = require("@nathanfaucett/material"),
    WebGLPlugin = require("@nathanfaucett/webgl_plugin"),
    SpriteRenderer = require("@nathanfaucett/sprite_renderer"),
    Input = require("@nathanfaucett/input"),
    Texture = require("@nathanfaucett/texture"),
    states = require("./enums/states"),
    tools = require("./enums/tools"),
    Pointers = require("./Pointers"),
    CanvasTexture = require("./CanvasTexture"),
    Canvas = require("./Canvas"),
    Canvas2D = require("./Canvas2D");


var MOUSE_ID = "MOUSE_ID",
    WIDTH = 512,
    HEIGHT = 512,

    Scene = sceneGraph.Scene,
    Entity = sceneGraph.Entity,

    SceneRenderer = sceneRenderer.SceneRenderer,

    Camera = cameraComponent.Camera,
    Sprite = spriteComponent.Sprite,
    Transform2D = transformComponents.Transform2D,

    shader = Shader.create({
        name: "shader_simple",
        src: null,
        vertex: [
            "varying vec2 vUv;",

            "void main(void) {",
            "   vUv = getUV();",
            "   gl_Position = perspectiveMatrix * modelViewMatrix * getPosition();",
            "}"
        ].join("\n"),
        fragment: [
            "uniform sampler2D texture;",

            "varying vec2 vUv;",

            "void main(void) {",
            "    gl_FragColor = texture2D(texture, vec2(vUv.s, vUv.t));",
            "}"
        ].join("\n")
    }),

    ClassPrototype = Class.prototype,
    EditorPrototype;


module.exports = Editor;


function Editor() {
    var _this = this;

    Class.call(this);

    this.element = null;
    this.canvas = null;
    this.scene = null;
    this.renderer = null;
    this.input = null;
    this.pointers = null;

    this._offScreenCanvas = null;
    this._canvases = null;

    this.onEvent = debounce.requestAnimationFrame(function onEvent() {
        var scene = _this.scene;

        _this.input.update(scene.time.current, scene.time.frame);
        _this.scene.update();
        _this.renderer.render();
    });

    this.onMouseMove = function(e, mouse) {
        _this.pointers.setPointerPosition(
            MOUSE_ID,
            mouse.position[0],
            mouse.position[1]
        );
    };
    this.onMouseDown = function() {
        _this.pointers.setPointerForce(MOUSE_ID, 1);
    };

    this.onMouseUp = function() {
        _this.pointers.setPointerForce(MOUSE_ID, 0);
    };
    this.onMouseLeave = function() {
        _this.pointers.setPointerCancel(MOUSE_ID);
    };
    this.onPointerState = function(pointer, toolState) {
        return onPointerState(_this, pointer, toolState);
    };
}
Class.extend(Editor, "canvas_editor.Editor");
EditorPrototype = Editor.prototype;

EditorPrototype.construct = function(element) {
    if (!isNode(element)) {
        new TypeError("Editor.construct(element: Element) Invalid element " + element);
    }
    var scene = Scene.create(),

        camera = Camera.create(),
        cameraTransform = Transform2D.create(),

        renderer = SceneRenderer.create(scene),

        spriteRenderer = SpriteRenderer.create(),
        webglPlugin = WebGLPlugin.create(),

        input = Input.create(),
        pointers = Pointers.create(),

        canvas = Canvas.create(element.ownerDocument, WIDTH, HEIGHT);

    ClassPrototype.construct.call(this);

    element.style.width = WIDTH + "px";
    element.style.height = HEIGHT + "px";
    element.appendChild(canvas.element);

    camera.set(WIDTH, HEIGHT);
    vec4.set(camera.background, 1, 1, 1, 1);
    camera.setOrthographic(true);

    scene.addEntity(Entity.create().addComponent(cameraTransform, camera));

    webglPlugin.setCanvas(canvas.element);
    spriteRenderer.setMode2D(true);

    renderer.addPlugin(webglPlugin);
    renderer.addRenderer(spriteRenderer);

    input.setElement(element);

    this.element = element;
    this.canvas = canvas;
    this.scene = scene;
    this.renderer = renderer;
    this.input = input;
    this.pointers = pointers;

    this._offScreenCanvas = Canvas2D.create(element.ownerDocument, WIDTH, HEIGHT);
    this._canvases = {};

    input.on("mousemove", this.onMouseMove);
    input.on("mousedown", this.onMouseDown);
    input.on("mouseup", this.onMouseUp);
    input.on("mouseout", this.onMouseLeave);

    input._handler.on("event", this.onEvent);

    pointers.on("pointer-state", this.onPointerState);

    scene.init();
    renderer.init();

    scene.update();
    renderer.render();

    return this;
};

EditorPrototype.destructor = function() {

    ClassPrototype.destructor.call(this);

    this.scene.destructor();
    this.renderer.destructor();

    input.off("mousemove", this.onMouseMove);
    input.off("mousedown", this.onMouseDown);
    input.off("mouseup", this.onMouseUp);
    input.off("mouseout", this.onMouseLeave);

    this.input._handler.off("event", this.onEvent);
    this.input.destructor();

    pointers.off("pointer-state", this.onPointerState);

    this._offScreenCanvas.destructor();

    this.element = null;
    this.canvas = null;
    this.scene = null;
    this.renderer = null;
    this.input = null;
    this.pointers = null;

    this._offScreenCanvas = null;
    this._canvases = null;

    return this;
};

function createCanvasEntity(_this, id) {
    var entity = Entity.create().addComponent(
        Transform2D.create(),
        Sprite.create({
            width: 2,
            height: 2,
            material: Material.create({
                name: "material_simple",
                src: null,
                shader: shader,
                wireframe: false,
                wireframeLineWidth: 1,
                uniforms: {
                    texture: CanvasTexture.create({
                        name: "canvas_texture",
                        flipY: true,
                        width: WIDTH,
                        height: HEIGHT
                    })
                }
            })
        })
    );

    _this._canvases[id] = entity;
    _this.scene.addEntity(entity);

    return entity;
}

function createLayer(_this, entity, canvasTexture, canvas, pointer, toolState) {
    var aabb = toolState.aabb,
        x = aabb.min[0] - toolState.width,
        y = aabb.min[1] - toolState.width,
        w = aabb.max[0] + toolState.width - x,
        h = aabb.max[1] + toolState.width - y,
        data = canvas.getImageData(x, y, w, h),
        image = new Image();

    _this._offScreenCanvas.putImageData(data, 0, 0, w, h);
    image.src = _this._offScreenCanvas.toDataURL();

    image.onload = function onLoad() {
        _this.scene.addEntity(Entity.create().addComponent(
            Transform2D.create(),
            Sprite.create({
                width: 2,
                height: 2,
                material: Material.create({
                    name: "material-" + image.src,
                    src: null,
                    shader: shader,
                    wireframe: false,
                    wireframeLineWidth: 1,
                    uniforms: {
                        texture: Texture.create({
                            name: "texture-" + image.src,
                            flipY: true,
                            data: image,
                            width: w,
                            height: h
                        })
                    }
                })
            })
        ));
        _this.scene.removeEntity(entity);
    };
}

function onPointerState(_this, pointer, toolState) {
    var entity, canvasTexture, canvas, position, lastPosition;

    switch (toolState.state) {

        case states.START:
            createCanvasEntity(_this, pointer.id);
            break;

        case states.RUN:
            entity = _this._canvases[pointer.id];
            canvasTexture = entity.getComponent(Sprite.className).material.uniforms.texture;
            canvas = canvasTexture.canvas;
            position = pointer.position;

            switch (toolState.tool) {
                case tools.BRUSH:
                    lastPosition = toolState.last;

                    canvas.drawLine(
                        lastPosition[0],
                        lastPosition[1],
                        position[0],
                        position[1],
                        toolState.width,
                        toolState.color
                    );
                    canvasTexture.forceUpdate();
                    break;
                case tools.LINE:
                    lastPosition = toolState.start;

                    canvas.clear();
                    canvas.drawLine(
                        lastPosition[0],
                        lastPosition[1],
                        position[0],
                        position[1],
                        toolState.width,
                        toolState.color
                    );
                    canvasTexture.forceUpdate();
                    break;
            }
            break;

        case states.END:
            entity = _this._canvases[pointer.id];
            canvasTexture = entity.getComponent(Sprite.className).material.uniforms.texture;
            canvas = canvasTexture.canvas;
            delete _this._canvases[pointer.id];
            createLayer(_this, entity, canvasTexture, canvas, pointer, toolState);
            break;
    }
}