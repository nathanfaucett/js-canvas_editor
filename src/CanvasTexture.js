var Texture = require("@nathanfaucett/texture"),
    Canvas2D = require("./Canvas2D");


var TexturePrototype = Texture.prototype,
    CanvasTexturePrototype;


module.exports = CanvasTexture;


function CanvasTexture() {
    Texture.call(this);
}
Texture.extend(CanvasTexture, "canvas_editor.CanvasTexture");
CanvasTexturePrototype = CanvasTexture.prototype;

CanvasTexturePrototype.construct = function(options) {

    TexturePrototype.construct.call(this, options);

    this.src = null;
    this.canvas = Canvas2D.create(options.document || document, this.width, this.height);
    this.data = this.canvas.element;

    return this;
};

CanvasTexturePrototype.destructor = function() {

    TexturePrototype.destructor.call(this);

    this.canvas.destructor();
    this.data = null;

    return this;
};

CanvasTexturePrototype.forceUpdate = function() {
    this.emitArg("update");
    return this;
};