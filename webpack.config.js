module.exports = {
    cache: false,
    watch: true,

    entry: {
        "simple": "./examples/simple/src/index.js"
    },

    output: {
        filename: "./examples/simple/index.js"
    },

    module: {
        loaders: [
            { test: /\.js$/, loader: "babel-loader" },
        ]
    },
    devtool: "source-map"
};
